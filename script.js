let marketingDiv = document.getElementById("marketingDiv");
let progDiv = document.getElementById("programiranjeDiv");
let designDiv = document.getElementById("designDiv");
let rootDiv = document.getElementById("rootDiv");

marketingDiv.addEventListener("click", function(){
    marketingDiv.classList.toggle("activeClassRed");
    marketingDiv.classList.toggle("bg-dark");

    progDiv.classList.add("bg-dark");
    progDiv.classList.remove("activeClassRed");
    designDiv.classList.add("bg-dark");
    designDiv.classList.remove("activeClassRed");

    document.querySelectorAll(".marketingClass").forEach(function(el){
        el.classList.remove("hideClass");
    });
    document.querySelectorAll(".designClass").forEach(function(el){
        el.classList.add("hideClass");
    });
    document.querySelectorAll(".codeClass").forEach(function(el){
        el.classList.add("hideClass");
    });


});

progDiv.addEventListener("click", function(){
    progDiv.classList.toggle("activeClassRed");
    progDiv.classList.toggle("bg-dark");

    marketingDiv.classList.add("bg-dark");
    marketingDiv.classList.remove("activeClassRed");
    designDiv.classList.add("bg-dark");
    designDiv.classList.remove("activeClassRed");

    document.querySelectorAll(".codeClass").forEach(function(el){
        el.classList.remove("hideClass");
    });
    document.querySelectorAll(".marketingClass").forEach(function(el){
        el.classList.add("hideClass");
    });
    document.querySelectorAll(".designClass").forEach(function(el){
        el.classList.add("hideClass");
    });
    
});

designDiv.addEventListener("click", function(){
    designDiv.classList.toggle("activeClassRed");
    designDiv.classList.toggle("bg-dark");

    marketingDiv.classList.add("bg-dark");
    marketingDiv.classList.remove("activeClassRed");
    progDiv.classList.add("bg-dark");
    progDiv.classList.remove("activeClassRed");


    document.querySelectorAll(".designClass").forEach(function(el){
        el.classList.remove("hideClass");
    });
    document.querySelectorAll(".marketingClass").forEach(function(el){
        el.classList.add("hideClass");
    });
    document.querySelectorAll(".codeClass").forEach(function(el){
        el.classList.add("hideClass");
    });
    
});